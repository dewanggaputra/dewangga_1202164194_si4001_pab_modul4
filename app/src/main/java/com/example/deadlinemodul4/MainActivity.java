package com.example.deadlinemodul4;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.LauncherActivity;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.menu.MenuAdapter;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.firebase.ui.database.SnapshotParser;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FileDownloadTask;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Objects;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "MainActivity";
    private static final int ADD_DATA_REQUEST_CODE = 4410;
    private ViewHolder viewHolder;
    private DatabaseReference mDatabase;
    private RecyclerView recyclerView;
    private MenuAdapter adapter;
    private ArrayList<Menu> menus = new ArrayList<>();
    private RecyclerView.LayoutManager layoutManager;
    private FloatingActionButton fab;
    private FirebaseRecyclerAdapter firebase;
    private StorageReference mStorageRef;
    private CoordinatorLayout coord_main;
    private String imageURL;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.AppTheme);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mDatabase = FirebaseDatabase.getInstance().getReference("menu").push();
        recyclerView = findViewById(R.id.recycler_main);
        layoutManager = new LinearLayoutManager(MainActivity.this);
        mStorageRef = FirebaseStorage.getInstance().getReference();
        coord_main = findViewById(R.id.coord_main);
        fetch();
        mDatabase.addValueEventListener(new ValueEventListener() { //Useless at this current form. For AsyncTask
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                MenuAbstract menuAbstract = dataSnapshot.getValue(MenuAbstract.class);
                //todo: Update data asynchronously... if it hasn't happened. I really believe it's stable enough.
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
        fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                add_data();
            }
        });

    }

    @Override
    public void onBackPressed() { //Tindakan ketika tombol back ditekan
        super.onBackPressed();
        new AlertDialog.Builder(this)
                .setTitle("Menutup aplikasi")
                .setMessage("Apakah anda yakin akan menutup aplikasi ini?")
                .setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        FirebaseAuth.getInstance().signOut(); //Melakukan logout pada Firebase
                        finish();
                    }
                }).setNegativeButton("Tidak", null)
                .show();
    }

    private void fetch() {
        Query query = FirebaseDatabase.getInstance()
                .getReference()
                .child("menu");
        FirebaseRecyclerOptions<MenuAbstract> options =
                new FirebaseRecyclerOptions.Builder<MenuAbstract>()
                        .setQuery(query, new SnapshotParser<MenuAbstract>() {
                            @NonNull
                            @Override
                            public MenuAbstract parseSnapshot(@NonNull DataSnapshot snapshot) {
                                Log.d(TAG, snapshot.child("nama").getValue().toString());
                                imageURL = snapshot.child("image").getValue().toString();
                                return new MenuAbstract
                                        (snapshot.child("nama").getValue().toString(),
                                                snapshot.child("image").getValue().toString(),
                                                snapshot.child("harga").getValue().toString(),
                                                snapshot.child("desc").getValue().toString());
                            }
                        }).build();

        firebase = new FirebaseRecyclerAdapter<MenuAbstract, ViewHolder>(options) {
            @NonNull
            @Override
            public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
                View view = LayoutInflater.from(viewGroup.getContext())
                        .inflate(R.layout.card_list, viewGroup, false);
                return new ViewHolder(view);
            }

            @Override
            protected void onBindViewHolder(@NonNull ViewHolder holder, final int position, @NonNull final MenuAbstract model) {
                holder.setmNama(model.getName());
                holder.setmHarga(model.getHarga());
                holder.setmDeskripsi(model.getDesc());
                Picasso.with(MainActivity.this)
                        .load(model.getImage())
                        .into(holder.getmImage_pp());
            }
        };

        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(firebase);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == ADD_DATA_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                Snackbar.make(coord_main, "Data berhasil ditambahkan!", Snackbar.LENGTH_LONG)
                        .show();
            }
        }
    }

    private void add_data() {
        Intent intent = new Intent(MainActivity.this, NewMenuActivity.class);
        startActivityForResult(intent, ADD_DATA_REQUEST_CODE);
    }

    @Override
    protected void onStart() {
        super.onStart();
        firebase.startListening();
    }

    @Override
    protected void onStop() {
        super.onStop();
        firebase.stopListening();
    }
}

class MenuAbstract {
    public String name;
    public String image;
    public String harga;
    public String desc;

    public MenuAbstract() {
    }

    public MenuAbstract(String name, String image, String harga, String desc) {
        this.name = name;
        this.image = image;
        this.harga = harga;
        this.desc = desc;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getHarga() {
        return harga;
    }

    public void setHarga(String harga) {
        this.harga = harga;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}


class ViewHolder extends RecyclerView.ViewHolder {
    protected CoordinatorLayout root;
    private TextView mNama, mHarga, mDeskripsi;
    private ImageView mImage_pp;
    private Drawable drawable;

    public ViewHolder(@NonNull View itemView) {
        super(itemView);
        root = itemView.findViewById(R.id.coord_main);
        mNama = itemView.findViewById(R.id.txt_nama_makanan);
        mHarga = itemView.findViewById(R.id.txt_harga_makanan);
        mDeskripsi = itemView.findViewById(R.id.txt_desc_makanan);
        mImage_pp = itemView.findViewById(R.id.img_pp_makanan);
    }

    public TextView getmNama() {
        return mNama;
    }

    public void setmImage_pp(Drawable drawable) {
        mImage_pp.setImageDrawable(drawable);
    }

    public void setmNama(TextView mNama) {
        this.mNama = mNama;
    }

    public TextView getmHarga() {
        return mHarga;
    }

    public void setmHarga(TextView mHarga) {
        this.mHarga = mHarga;
    }

    public TextView getmDeskripsi() {
        return mDeskripsi;
    }

    public void setmDeskripsi(TextView mDeskripsi) {
        this.mDeskripsi = mDeskripsi;
    }

    public ImageView getmImage_pp() {
        return mImage_pp;
    }

    public void setmImage_pp(ImageView mImage_pp) {
        this.mImage_pp = mImage_pp;
    }

    public void setmNama(String nama) {
        mNama.setText(nama);
    }


    public void setmHarga(String harga) {
        mHarga.setText(harga);
    }


    public void setmDeskripsi(String deskripsi) {
        mDeskripsi.setText(deskripsi);
    }

}
