package com.example.deadlinemodul4;

public class Menu {
    public String name;
    public String image;
    public String harga;
    public String desc;

    public Menu() {
    }

    public Menu(String name, String image, String harga, String desc) {
        this.name = name;
        this.image = image;
        this.harga = harga;
        this.desc = desc;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getHarga() {
        return harga;
    }

    public void setHarga(String harga) {
        this.harga = harga;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
